import React, { useState} from "react";
import './Create.css';
import axios from "axios";

function Create() {
    const [task, setTask] =useState()
   
    const handleKeyDown = (e) => {
      if (e.key === 'Enter') {
          handleAdd();
      }
  };

    const handleAdd = () => {
      /// Check whether the task already exists in the database
      axios.get('http://localhost:3001/get')
      .then(response => {
          const tasks = response.data; // Assumption that the data is returned as an array of objects
          const foundTask = tasks.find(dbTask => dbTask.task === task);

          if (foundTask) {
              // The task already exists, display an error message
              alert("The Task is already exist")
          } else {
              // The task does not exist, add it
              axios.post('http://localhost:3001/add', { task: task })
              .then(result => {
                  window.location.reload();
              })
              .catch(err => console.error(err));
          }
      })
      .catch(err => console.error(err));
  }


    return (
      <>
      <div className="create_form">
        <input 
        type="text" 
        name="Input" 
        placeholder="Input the Task" 
        value={task}   
        id="" className="input" 
        onKeyDown={handleKeyDown}
        onChange={(e)=> setTask(e.target.value)}></input>
        <button type="button" className="button" onClick={handleAdd}>Add</button>
      </div>
      </>
    );
  }
export default Create;
