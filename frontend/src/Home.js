import React, { useEffect, useState } from "react";
import Create from "./Create";
import axios from "axios";
import { BsFillTrashFill} from "react-icons/bs";
import './Home.css';

function Home() {
    const [todos, setTodos] =useState([])

    //Rest API get the list data from datebase
    useEffect(()=>{
        axios.get('http://localhost:3001/get')
        .then(result=>setTodos(result.data))
        .catch(err=>console.log(err))
    }, [])

    //delete the data but with the confirmation
    const handleDelete = (id) => {
      const confirmation = window.confirm("Are you sure you want to delete this task?");
      
      if (confirmation) {
          axios.delete(`http://localhost:3001/delete/${id}`)
          .then(result => {
              window.location.reload();
          })
          .catch(err => console.log(err))
      }
  }


    return (
      <>
      <div>
        <h1 className="Titel">ToDo List</h1>

        
        <Create/>
        {
            todos.length===0
            ?
            <div><h2>No Record</h2></div>
            :
            todos.map(todo =>(
                <div className="Todo">
                <div className="task">    
                  <p>{todo.task}</p>
                </div>

                <div>
                <span><BsFillTrashFill className="icon" onClick={()=>handleDelete(todo._id)}/></span>
                </div>

                </div>
            ))
        }
      </div>
      </>
    );
  }
export default Home;
