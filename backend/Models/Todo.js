const mongoose =require('mongoose')

//declare variable in the database
const TodoSchema =new mongoose.Schema({
    task: String,
})

const TodoModel=mongoose.model("todos", TodoSchema)
module.exports=TodoModel