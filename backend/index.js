const express = require('express')
const mongoose =require('mongoose')
const cors =require('cors')
const TodoModel =require('./Models/Todo')

const app=express()
app.use(cors())
app.use(express.json())

//connect the database
mongoose.connect("mongodb://127.0.0.1:27017/TodosList")

//Rest API get
app.get('/get', (req,res)=>{
    TodoModel.find()
    .then(result=>{res.json(result), console.log("success list data")})
    .catch(err=> res.json(err))
})

//Rest API delete
app.delete('/delete/:id', (req, res)=>{
    const {id} =req.params;
    TodoModel.findByIdAndDelete({_id:id})
    .then(result=>{res.json(result), console.log("success delete data")})
    .catch(err=> res.json(err))
})

//Rest API post
app.post('/add', (req, res)=>{
    const task=req.body.task;
    TodoModel.create({
        task:task
    }).then(result=>{res.json(result), console.log("success add data")})
    .catch(err=>res.json(err))

})
//connect host and port
app.listen(3001,()=>{
    console.log("Server is Running")
})